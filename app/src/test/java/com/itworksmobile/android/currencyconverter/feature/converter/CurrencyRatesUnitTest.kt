package com.itworksmobile.android.currencyconverter.feature.converter

import com.itworksmobile.android.currencyconverter.MockRepository
import com.itworksmobile.android.currencyconverter.extension.newBigDecimal
import com.itworksmobile.android.currencyconverter.feature.converter.model.CurrencyRates
import org.junit.Assert
import org.junit.Test
import java.math.BigDecimal

/**
 * Created by Naglis Žemaitis on 2019-10-20.
 * ItWorks Mobile. http://itworksmobile.com/
 */
class CurrencyRatesUnitTest {

    private val response = MockRepository.getEurCurrencyRatesResponse()
    private val currencies = MockRepository.getEurCurrencies()

    // update
    @Test
    fun `update empty list by response`() {
        val emptyCurrencyRates = CurrencyRates()

        emptyCurrencyRates.update(response)

        Assert.assertEquals(3, emptyCurrencyRates.currencies.count())
        Assert.assertEquals("EUR", emptyCurrencyRates.selectedCurrency)
        Assert.assertEquals("EUR", emptyCurrencyRates.currencies[0].abbreviation)
        Assert.assertEquals("GBP", emptyCurrencyRates.currencies[2].abbreviation)
    }

    @Test
    fun `update filled list by response`() {
        val currencyRates = CurrencyRates(currencies = currencies)

        Assert.assertEquals("EUR", currencyRates.selectedCurrency)

        currencyRates.update(response)

        Assert.assertEquals(3, currencyRates.currencies.count())
        Assert.assertEquals("EUR", currencyRates.currencies[0].abbreviation)
        Assert.assertEquals(1f, currencyRates.currencies[0].rate.toFloat())
        Assert.assertEquals("GBP", currencyRates.currencies[1].abbreviation)
        Assert.assertEquals(1.2f, currencyRates.currencies[1].rate.toFloat())
        Assert.assertEquals("USD", currencyRates.currencies[2].abbreviation)
        Assert.assertEquals(0.8f, currencyRates.currencies[2].rate.toFloat())
    }

    @Test
    fun `update filled list by response with wrong base`() {
        val currencyRates = CurrencyRates("USD", currencies = currencies)

        currencyRates.update(response)

        Assert.assertEquals("USD", currencyRates.selectedCurrency)
        Assert.assertEquals("USD", currencyRates.currencies[0].abbreviation)
        Assert.assertEquals(0.9f, currencyRates.currencies[0].rate.toFloat())
        Assert.assertEquals("GBP", currencyRates.currencies[1].abbreviation)
        Assert.assertEquals(1.1f, currencyRates.currencies[1].rate.toFloat())
        Assert.assertEquals("EUR", currencyRates.currencies[2].abbreviation)
        Assert.assertEquals(1f, currencyRates.currencies[2].rate.toFloat())
    }

    @Test
    fun `update filled list by response with inputted value`() {
        val currencyRates = CurrencyRates("EUR", currencies = currencies)

        currencyRates.updateSelectedCurrencyInput("100.33")
        currencyRates.update(response)

        Assert.assertEquals("EUR", currencyRates.currencies[0].abbreviation)
        Assert.assertEquals(1f, currencyRates.currencies[0].rate.toFloat())
        Assert.assertEquals(100.33f, currencyRates.currencies[0].inputValue.toFloat())
        Assert.assertEquals("GBP", currencyRates.currencies[1].abbreviation)
        Assert.assertEquals(1.2f, currencyRates.currencies[1].rate.toFloat())
        Assert.assertEquals(
            newBigDecimal(100.33 * 1.2).toFloat(),
            currencyRates.currencies[1].inputValue.toFloat()
        )
        Assert.assertEquals("USD", currencyRates.currencies[2].abbreviation)
        Assert.assertEquals(0.8f, currencyRates.currencies[2].rate.toFloat())
        Assert.assertEquals(
            newBigDecimal(100.33 * 0.8).toFloat(),
            currencyRates.currencies[2].inputValue.toFloat()
        )
    }

    // updateSelectedCurrencyInput
    @Test
    fun `update selected currency input with corrupted value`() {
        val currencyRates = CurrencyRates(currencies = currencies)

        currencyRates.updateSelectedCurrencyInput("abc")

        Assert.assertEquals("EUR", currencyRates.selectedCurrency)
        Assert.assertEquals(BigDecimal(0), currencyRates.currencies[0].inputValue)
    }

    @Test
    fun `update selected currency input`() {
        val currencyRates = CurrencyRates(currencies = currencies)

        currencyRates.updateSelectedCurrencyInput("100.0033")

        Assert.assertEquals("EUR", currencyRates.selectedCurrency)
        Assert.assertEquals("100", currencyRates.currencies[0].inputValue.toPlainString())
        Assert.assertEquals("110", currencyRates.currencies[1].inputValue.toPlainString())
        Assert.assertEquals("90", currencyRates.currencies[2].inputValue.toPlainString())

        currencyRates.updateSelectedCurrencyInput("10")

        Assert.assertEquals("10", currencyRates.currencies[0].inputValue.toPlainString())
        Assert.assertEquals("11", currencyRates.currencies[1].inputValue.toPlainString())
        Assert.assertEquals("9", currencyRates.currencies[2].inputValue.toPlainString())

        currencyRates.updateSelectedCurrencyInput("11.899")

        Assert.assertEquals("11.9", currencyRates.currencies[0].inputValue.toPlainString())

    }

    // updateSelectedCurrency
    @Test
    fun `update selected currency with corrupted index`() {
        val currencyRates = CurrencyRates(currencies = currencies)

        currencyRates.updateSelectedCurrency(123)

        Assert.assertEquals("EUR", currencyRates.selectedCurrency)

        currencyRates.updateSelectedCurrency(-23)

        Assert.assertEquals("EUR", currencyRates.selectedCurrency)
    }

    @Test
    fun `update selected currency`() {
        val currencyRates = CurrencyRates(currencies = currencies)

        currencyRates.updateSelectedCurrency(1)

        Assert.assertEquals("GBP", currencyRates.selectedCurrency)
    }

    @Test
    fun `successful real time scenario`() {
        val currencyRates = CurrencyRates(currencies = currencies)

        currencyRates.updateSelectedCurrencyInput("1000000")
        currencyRates.update(response)

        Assert.assertEquals("EUR", currencyRates.currencies[0].abbreviation)
        Assert.assertEquals(1f, currencyRates.currencies[0].rate.toFloat())
        Assert.assertEquals(1000000f, currencyRates.currencies[0].inputValue.toFloat())
        Assert.assertEquals("GBP", currencyRates.currencies[1].abbreviation)
        Assert.assertEquals(1.2f, currencyRates.currencies[1].rate.toFloat())
        Assert.assertEquals(
            newBigDecimal(1200000.0).toFloat(),
            currencyRates.currencies[1].inputValue.toFloat()
        )
        Assert.assertEquals("USD", currencyRates.currencies[2].abbreviation)
        Assert.assertEquals(0.8f, currencyRates.currencies[2].rate.toFloat())
        Assert.assertEquals(
            newBigDecimal(800000.0).toFloat(),
            currencyRates.currencies[2].inputValue.toFloat()
        )

        currencyRates.updateSelectedCurrency(2)

        Assert.assertEquals("USD", currencyRates.selectedCurrency)
        Assert.assertEquals("USD", currencyRates.currencies[0].abbreviation)
        Assert.assertEquals(0.8f, currencyRates.currencies[0].rate.toFloat())
        Assert.assertEquals(
            newBigDecimal(800000.0).toFloat(),
            currencyRates.currencies[0].inputValue.toFloat()
        )
        Assert.assertEquals("EUR", currencyRates.currencies[1].abbreviation)
        Assert.assertEquals(1f, currencyRates.currencies[1].rate.toFloat())
        Assert.assertEquals(1000000f, currencyRates.currencies[1].inputValue.toFloat())
        Assert.assertEquals("GBP", currencyRates.currencies[2].abbreviation)
        Assert.assertEquals(1.2f, currencyRates.currencies[2].rate.toFloat())
        Assert.assertEquals(
            newBigDecimal(1200000.0).toFloat(),
            currencyRates.currencies[2].inputValue.toFloat()
        )

        val usdResponse = MockRepository.getUsdCurrencyRatesResponse()
        currencyRates.update(usdResponse)

        Assert.assertEquals("USD", currencyRates.selectedCurrency)
        Assert.assertEquals("USD", currencyRates.currencies[0].abbreviation)
        Assert.assertEquals(1f, currencyRates.currencies[0].rate.toFloat())
        Assert.assertEquals(
            newBigDecimal(800000.0).toFloat(),
            currencyRates.currencies[0].inputValue.toFloat()
        )
        Assert.assertEquals("EUR", currencyRates.currencies[1].abbreviation)
        Assert.assertEquals(1.27f, currencyRates.currencies[1].rate.toFloat())
        Assert.assertEquals(1016000f, currencyRates.currencies[1].inputValue.toFloat())
        Assert.assertEquals("GBP", currencyRates.currencies[2].abbreviation)
        Assert.assertEquals(1.82f, currencyRates.currencies[2].rate.toFloat())
        Assert.assertEquals(
            newBigDecimal(1456000.0).toFloat(),
            currencyRates.currencies[2].inputValue.toFloat()
        )
    }

}