package com.itworksmobile.android.currencyconverter

import com.itworksmobile.android.currencyconverter.api.entity.CurrencyRatesResponse
import com.itworksmobile.android.currencyconverter.feature.converter.model.Currency
import java.math.BigDecimal

/**
 * Created by Naglis Žemaitis on 2019-10-19.
 * ItWorks Mobile. http://itworksmobile.com/
 */
object MockRepository {

    fun getUsdCurrencyRatesResponse() = CurrencyRatesResponse(
        "USD",
        "2020-12-20",
        mutableMapOf<String, Double>().apply {
            put("EUR", 1.27)
            put("GBP", 1.82)
        }
    )

    fun getEurCurrencyRatesResponse() = CurrencyRatesResponse(
        "EUR",
        "2020-12-20",
        mutableMapOf<String, Double>().apply {
            put("USD", 0.8)
            put("GBP", 1.2)
        }
    )

    fun getEurCurrencies() = mutableListOf<Currency>().apply {
        add(Currency(
            "GBP",
            BigDecimal(1.1),
            BigDecimal.ZERO
        ))
        add(Currency(
            "USD",
            BigDecimal(0.9),
            BigDecimal.ZERO
        ))
        add(Currency(
            "EUR",
            BigDecimal.ONE,
            BigDecimal.ZERO
        ))
    }
}