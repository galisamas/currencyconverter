package com.itworksmobile.android.currencyconverter.feature

import com.itworksmobile.android.currencyconverter.feature.converter.model.Currency
import java.math.BigDecimal

/**
 * Created by Naglis Žemaitis on 2019-10-19.
 * ItWorks Mobile. http://itworksmobile.com/
 */
object MockRepository {

    fun getCurrency(abbreviation: String = "EUR") = Currency(
        abbreviation,
        BigDecimal.ONE,
        BigDecimal.ZERO

    )
}