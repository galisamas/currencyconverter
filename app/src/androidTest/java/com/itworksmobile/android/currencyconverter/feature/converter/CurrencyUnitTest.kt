package com.itworksmobile.android.currencyconverter.feature.converter

import android.content.Context
import androidx.test.platform.app.InstrumentationRegistry
import com.itworksmobile.android.currencyconverter.R
import com.itworksmobile.android.currencyconverter.feature.MockRepository
import org.junit.Assert
import org.junit.Before
import org.junit.Test

/**
 * Created by Naglis Žemaitis on 2019-10-24.
 * ItWorks Mobile. http://itworksmobile.com/
 */
class CurrencyUnitTest {

    private lateinit var context: Context

    private val eurCurrency = MockRepository.getCurrency()
    private val unknownCurrency = MockRepository.getCurrency("QQQ")

    @Before
    fun setUp() {
        context = InstrumentationRegistry.getInstrumentation().targetContext
    }

    @Test
    fun defaultDrawable() {
        Assert.assertEquals(R.drawable.ic_unknown, unknownCurrency.getFlagRes(context))
    }

    @Test
    fun correctDrawable() {
        Assert.assertEquals(R.drawable.eur_flag, eurCurrency.getFlagRes(context))
    }

    @Test
    fun defaultStrings() {
        Assert.assertEquals(R.string.unknown_abbreviation, unknownCurrency.getTitleRes(context))
        Assert.assertEquals(R.string.unknown_full_title, unknownCurrency.getDetailsRes(context))
    }

    @Test
    fun correctStrings() {
        Assert.assertEquals(R.string.eur_abbreviation, eurCurrency.getTitleRes(context))
        Assert.assertEquals(R.string.eur_full_title, eurCurrency.getDetailsRes(context))
    }
}