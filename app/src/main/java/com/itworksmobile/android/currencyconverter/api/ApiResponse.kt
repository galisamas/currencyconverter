package com.itworksmobile.android.rallyapp.api

/**
 * Created by Naglis Žemaitis on 2019-09-23.
 * ItWorks Mobile. http://itworksmobile.com/
 */
import retrofit2.Response
import java.net.UnknownHostException

class ApiResponse<R> {

    internal val responseCode: Int
    val response: R?
    val codeType: CodeType

    val isNetworkError: Boolean
        get() = codeType == CodeType.NETWORK_ERROR

    constructor(response: Response<R>) {
        responseCode = response.code()
        this.response = if (response.isSuccessful) response.body() else null
        codeType = getCodeType(responseCode)
    }

    constructor(throwable: Throwable) {
        responseCode = -1
        response = null

        codeType = if (throwable is UnknownHostException) CodeType.NETWORK_ERROR else CodeType.ERROR
    }

    fun get(success: ((R) -> Unit)? = null, failure: ((Any) -> Unit)? = null) {
        when {
            isSuccessful() && response != null -> success?.invoke(response)
            isSuccessful() -> failure?.invoke(Any())
            else -> failure?.invoke(Any())
        }
    }

    fun isSuccessful() = codeType == CodeType.SUCCESS

    private fun getCodeType(code: Int): CodeType = when {
        code in 200..299 -> CodeType.SUCCESS
        else -> CodeType.ERROR
    }

    enum class CodeType {
        SUCCESS,
        ERROR,
        NETWORK_ERROR
    }
}