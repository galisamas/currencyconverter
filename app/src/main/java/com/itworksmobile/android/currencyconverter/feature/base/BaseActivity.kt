package com.itworksmobile.android.currencyconverter.feature.base

import android.net.ConnectivityManager
import android.net.NetworkRequest
import androidx.appcompat.app.AppCompatActivity
import com.itworksmobile.android.currencyconverter.api.callback.NetworkCallback
import com.itworksmobile.android.currencyconverter.extension.isConnected

/**
 * Created by Naglis Žemaitis on 2019-10-19.
 * ItWorks Mobile. http://itworksmobile.com/
 */
abstract class BaseActivity: AppCompatActivity() {

    private val connectivityManager: ConnectivityManager by lazy {
        getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
    }
    private val networkCallback by lazy {
        NetworkCallback(connectivityManager) {
            runOnUiThread {
                onConnectivityChanged(it)
            }
        }
    }

    open val onConnectivityChanged: (Boolean) -> Unit = {}


    override fun onStart() {
        super.onStart()
        connectivityManager.registerNetworkCallback(
            NetworkRequest.Builder().build(),
            networkCallback
        )
    }

    override fun onStop() {
        connectivityManager.unregisterNetworkCallback(networkCallback)
        super.onStop()
    }

    override fun onResume() {
        super.onResume()
        if (!connectivityManager.isConnected())
            onConnectivityChanged(false)
    }
}