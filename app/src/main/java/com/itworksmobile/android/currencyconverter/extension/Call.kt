package com.itworksmobile.android.currencyconverter.extension

import com.itworksmobile.android.currencyconverter.api.callback.ApiCallback
import com.itworksmobile.android.rallyapp.api.ApiResponse
import retrofit2.Call

/**
 * Created by Naglis Žemaitis on 2019-10-20.
 * ItWorks Mobile. http://itworksmobile.com/
 */

fun <R> Call<R>.enqueue(callback: (ApiResponse<R>) -> Unit) {
    ApiCallback(this).enqueue { callback(it) }
}