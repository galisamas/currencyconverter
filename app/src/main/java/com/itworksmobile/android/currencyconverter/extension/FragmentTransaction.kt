package com.itworksmobile.android.currencyconverter.extension

import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Lifecycle

/**
 * Created by Naglis Žemaitis on 2019-10-19.
 * ItWorks Mobile. http://itworksmobile.com/
 */

fun FragmentTransaction.safeCommit(lifecycleState: Lifecycle.State) =
    if (lifecycleState.isAtLeast(Lifecycle.State.RESUMED))
        commit()
    else
        commitAllowingStateLoss()