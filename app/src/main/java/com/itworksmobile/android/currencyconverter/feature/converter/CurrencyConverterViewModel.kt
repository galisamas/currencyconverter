package com.itworksmobile.android.currencyconverter.feature.converter

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.itworksmobile.android.currencyconverter.api.endpoint.CurrencyEndpoint
import com.itworksmobile.android.currencyconverter.extension.enqueue
import com.itworksmobile.android.currencyconverter.feature.converter.model.CurrencyRates
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

/**
 * Created by Naglis Žemaitis on 2019-10-19.
 * ItWorks Mobile. http://itworksmobile.com/
 */
class CurrencyConverterViewModel(
    private val currencyEndpoint: CurrencyEndpoint
) : ViewModel() {

    private val currencyRates = CurrencyRates()

    private var refreshingJob: Job? = null

    val onCurrencyChangedListener = MutableLiveData<CurrencyRates>()
    val onFailureListener = MutableLiveData<Any>()

    fun startCurrencyUpdates() {
        refreshingJob = viewModelScope.launch(Dispatchers.IO) {
            refreshCurrencies()
        }
    }

    fun terminateUpdates() = refreshingJob?.cancel()

    fun updateSelectedCurrency(index: Int) = currencyRates.updateSelectedCurrency(index)

    fun updateSelectedCurrencyInput(newValue: String) = currencyRates.apply {
        updateSelectedCurrencyInput(newValue)
    }

    private suspend fun refreshCurrencies(delayInMillis: Long = DEFAULT_DELAY) {
        getCurrencies()
        delay(delayInMillis)
        refreshCurrencies(delayInMillis)
    }

    private fun getCurrencies() {
        val currencyName = currencyRates.selectedCurrency
        currencyEndpoint.getLatestRates(currencyName).enqueue { apiResponse ->
            apiResponse.get(success = { response ->
                currencyRates.update(response)
                onCurrencyChangedListener.postValue(currencyRates)
            }, failure = {
                onFailureListener.postValue(Any())
            })
        }
    }

    companion object {
        private const val DEFAULT_DELAY = 1_000L
    }
}