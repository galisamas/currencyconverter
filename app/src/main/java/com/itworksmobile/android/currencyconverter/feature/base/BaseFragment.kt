package com.itworksmobile.android.currencyconverter.feature.base

import android.net.ConnectivityManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.core.content.ContextCompat.getSystemService
import androidx.fragment.app.Fragment

/**
 * Created by Naglis Žemaitis on 2019-10-19.
 * ItWorks Mobile. http://itworksmobile.com/
 */
abstract class BaseFragment: Fragment() {

    val connectivityManager by lazy {
        context?.run {
            getSystemService(this, ConnectivityManager::class.java)
        }
    }

    @get:LayoutRes abstract val fragmentLayoutRes: Int

    final override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(fragmentLayoutRes, container, false)
}