package com.itworksmobile.android.currencyconverter.feature.converter.model

import com.itworksmobile.android.currencyconverter.api.entity.CurrencyRatesResponse
import com.itworksmobile.android.currencyconverter.extension.newBigDecimal
import java.math.BigDecimal

/**
 * Created by Naglis Žemaitis on 2019-10-20.
 * ItWorks Mobile. http://itworksmobile.com/
 */
data class CurrencyRates(
    var selectedCurrency: String = INIT_CURRENCY,
    val currencies: MutableList<Currency> = mutableListOf()
) {

    init {
        sortBySelected()
    }

    fun updateSelectedCurrency(index: Int) {
        currencies.getOrNull(index)?.let { newSelectedCurrency ->
            selectedCurrency = newSelectedCurrency.abbreviation
            sortBySelected()
        }
    }

    fun updateSelectedCurrencyInput(newValue: String) {
        val newInputValue = newValue.toDoubleOrNull() ?: 0.0
        currencies.firstOrNull { it.abbreviation == selectedCurrency }?.let {
            it.inputValue = newBigDecimal(newInputValue)
        }

        currencies
            .filterNot { it.abbreviation == selectedCurrency }
            .forEach {
                it.inputValue = newBigDecimal(newInputValue * it.rate.toDouble())
            }
    }

    fun update(response: CurrencyRatesResponse) = response.apply {
        if (currencies.isEmpty()) {
            initWithResponse(response)
            return@apply
        }

        if (selectedCurrency != base) return@apply

        val selectedCurrency = currencies.firstOrNull { it.abbreviation == base }

        selectedCurrency?.let {
            it.rate = BigDecimal.ONE
        }

        rates
            .filter { !it.key.isBlank() }
            .forEach { (title, newRate) ->
                currencies.firstOrNull { it.abbreviation == title }?.let {
                    it.rate = BigDecimal(newRate)
                    val selectedValue = selectedCurrency?.inputValue?.toDouble() ?: 1.0
                    it.inputValue = newBigDecimal(selectedValue * it.rate.toDouble())
                }
            }

        sortBySelected()
    }

    private fun initWithResponse(response: CurrencyRatesResponse) = response.apply {
        selectedCurrency = base
        currencies.add(Currency(
            base,
            newBigDecimal(1.0),
            BigDecimal.ONE
        ))

        rates
            .filter { !it.key.isBlank() }
            .forEach { (title, newRate) ->
                currencies.add(Currency(
                    title,
                    BigDecimal(newRate),
                    newBigDecimal(newRate)
                ))
            }

        sortBySelected()
    }

    private fun sortBySelected() = currencies.sortByDescending { it.abbreviation == selectedCurrency}

    companion object {
        private const val INIT_CURRENCY = "EUR"
    }
}