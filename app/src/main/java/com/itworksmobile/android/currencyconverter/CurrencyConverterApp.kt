package com.itworksmobile.android.currencyconverter

import android.app.Application
import com.itworksmobile.android.currencyconverter.di.Koin
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

/**
 * Created by Naglis Žemaitis on 2019-10-19.
 * ItWorks Mobile. http://itworksmobile.com/
 */
class CurrencyConverterApp : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@CurrencyConverterApp)
            modules(Koin.modules)
        }
    }
}