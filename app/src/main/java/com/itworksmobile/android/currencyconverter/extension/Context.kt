package com.itworksmobile.android.currencyconverter.extension

import android.content.Context
import android.widget.Toast
import androidx.annotation.StringRes

/**
 * Created by Naglis Žemaitis on 2019-10-19.
 * ItWorks Mobile. http://itworksmobile.com/
 */

fun Context.showToast(@StringRes titleRes: Int) =
    Toast.makeText(this, titleRes, Toast.LENGTH_SHORT).show()

fun Context.getIdentifier(resName: String, resourceType: String, fallbackRes: Int): Int {
    val resource = resources.getIdentifier(resName, resourceType, packageName)
    return if (resource <= 0) fallbackRes else resource

}