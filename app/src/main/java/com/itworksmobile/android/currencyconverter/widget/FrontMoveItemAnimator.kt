package com.itworksmobile.android.currencyconverter.widget

import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.RecyclerView

/**
 * Created by Naglis Žemaitis on 2019-10-21.
 * ItWorks Mobile. http://itworksmobile.com/
 */
class FrontMoveItemAnimator: DefaultItemAnimator() {

    override fun animateMove(
        holder: RecyclerView.ViewHolder?,
        fromX: Int,
        fromY: Int,
        toX: Int,
        toY: Int
    ): Boolean {
        val state = super.animateMove(holder, fromX, fromY, toX, toY)
        if (toY == 0) holder?.itemView?.z = DEFAULT_Z
        return state
    }

    override fun onMoveFinished(item: RecyclerView.ViewHolder?) {
        super.onMoveFinished(item)
        item?.itemView?.z = ZERO_Z
    }

    override fun getMoveDuration() = MOVED_DURATION

    companion object {
        private const val ZERO_Z = 0f
        private const val DEFAULT_Z = 1f
        private const val MOVED_DURATION = 400L
    }
}