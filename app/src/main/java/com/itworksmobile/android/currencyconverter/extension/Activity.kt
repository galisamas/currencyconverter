package com.itworksmobile.android.currencyconverter.extension

import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import com.itworksmobile.android.currencyconverter.feature.base.BaseFragment

/**
 * Created by Naglis Žemaitis on 2019-10-19.
 * ItWorks Mobile. http://itworksmobile.com/
 */

fun <T : BaseFragment> AppCompatActivity.replace(
    @IdRes containerId: Int,
    fragment: T,
    addToBackStack: Boolean = false,
    tag: String = fragment::class.java.name
) {
    val fragmentTransaction = supportFragmentManager.beginTransaction()

    fragmentTransaction.replace(containerId, fragment, tag)

    if (addToBackStack) {
        fragmentTransaction.addToBackStack(tag)
    }
    fragmentTransaction.safeCommit(lifecycle.currentState)
}