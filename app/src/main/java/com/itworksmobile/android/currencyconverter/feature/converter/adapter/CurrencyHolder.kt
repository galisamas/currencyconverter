package com.itworksmobile.android.currencyconverter.feature.converter.adapter

import android.view.ViewGroup
import com.itworksmobile.android.currencyconverter.R
import com.itworksmobile.android.currencyconverter.feature.base.BaseAdapter
import com.itworksmobile.android.currencyconverter.feature.base.BaseHolder
import com.itworksmobile.android.currencyconverter.feature.converter.model.Currency
import kotlinx.android.synthetic.main.holder_currency.view.*

/**
 * Created by Naglis Žemaitis on 2019-10-19.
 * ItWorks Mobile. http://itworksmobile.com/
 */
class CurrencyHolder(parent: ViewGroup) : BaseHolder<BaseAdapter.BaseItem<Currency>>(
    parent,
    R.layout.holder_currency
) {

    private val isFirst
        get() = adapterPosition == 0

    init {
        containerView.currencyView.apply {
            onClickListener = {
                onViewClick(R.id.currencyView, adapterPosition)
            }
            onTextChangeListener = {
                onViewClick(R.id.currencyEditText, it)
            }
        }
    }

    override fun onBind(listItem: BaseAdapter.BaseItem<Currency>) {
        containerView.currencyView.initView(listItem.item, isFirst)
    }

    fun updateCurrency(currency: Currency) =
        containerView.currencyView.updateInput(currency, isFirst)
}