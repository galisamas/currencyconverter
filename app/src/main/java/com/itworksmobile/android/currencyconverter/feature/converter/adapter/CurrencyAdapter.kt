package com.itworksmobile.android.currencyconverter.feature.converter.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.itworksmobile.android.currencyconverter.feature.base.BaseAdapter
import com.itworksmobile.android.currencyconverter.feature.base.BaseHolder
import com.itworksmobile.android.currencyconverter.feature.converter.model.Currency
import com.itworksmobile.android.currencyconverter.feature.converter.model.CurrencyRates
import com.itworksmobile.android.currencyconverter.widget.FrontMoveItemAnimator

/**
 * Created by Naglis Žemaitis on 2019-10-19.
 * ItWorks Mobile. http://itworksmobile.com/
 */
class CurrencyAdapter: BaseAdapter<BaseAdapter.BaseItem<Currency>>() {

    private var recyclerView: RecyclerView? = null

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        recyclerView.itemAnimator = FrontMoveItemAnimator()
        this.recyclerView = recyclerView
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        this.recyclerView = null
    }

    @Suppress("UNCHECKED_CAST")
    override fun <VH : BaseHolder<BaseItem<Currency>>> getViewHolder(parent: ViewGroup, viewType: Enum<*>): VH =
        CurrencyHolder(parent) as VH

    fun setCurrencyItems(rates: CurrencyRates) {
        items = rates.currencies.map { SingleTypeItem(it) }.toMutableList()
    }

    fun updateItems(rates: CurrencyRates) {
        rates.currencies.drop(1).forEach { currency ->
            val index = items.indexOfFirst { it.item.abbreviation == currency.abbreviation }
            (recyclerView?.findViewHolderForLayoutPosition(index) as? CurrencyHolder)?.updateCurrency(currency)
        }
    }

    fun moveItem(fromPosition: Int) {
        items.add(FIRST_ITEM_POSITION, items.removeAt(fromPosition))
        notifyItemMoved(fromPosition, FIRST_ITEM_POSITION)
        recyclerView?.post {
            recyclerView?.smoothScrollToPosition(FIRST_ITEM_POSITION)
        }
    }

    companion object {
        private const val FIRST_ITEM_POSITION = 0
    }
}