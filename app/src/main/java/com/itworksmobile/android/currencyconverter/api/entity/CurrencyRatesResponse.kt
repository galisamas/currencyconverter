package com.itworksmobile.android.currencyconverter.api.entity

/**
 * Created by Naglis Žemaitis on 2019-10-20.
 * ItWorks Mobile. http://itworksmobile.com/
 */
data class CurrencyRatesResponse(
    val base: String,
    val date: String,
    val rates: MutableMap<String, Double>
)