package com.itworksmobile.android.currencyconverter.widget

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.widget.doOnTextChanged
import com.bumptech.glide.Glide
import com.itworksmobile.android.currencyconverter.R
import com.itworksmobile.android.currencyconverter.feature.converter.model.Currency
import kotlinx.android.synthetic.main.view_currency.view.*

/**
 * Created by Naglis Žemaitis on 2019-10-19.
 * ItWorks Mobile. http://itworksmobile.com/
 */
class CurrencyView(context: Context, attrs: AttributeSet?) : ConstraintLayout(context, attrs) {

    private var notify = true

    var onTextChangeListener: (String) -> Unit = {}
    var onClickListener: () -> Unit = {}

    init {
        LayoutInflater.from(context).inflate(R.layout.view_currency, this, true)
        currencyEditText.doOnTextChanged { text, _, _, _ ->
            if (notify) {
                onTextChangeListener(text.toString())
            }
        }
        currencyEditText.setOnFocusChangeListener { _, focus ->
            if (focus) onClickListener.invoke()
        }
        container.setOnClickListener {
            onClickListener.invoke()
            currencyEditText.requestFocus()
        }
    }

    fun initView(currency: Currency, isCurrencyMain: Boolean) = currency.apply {
        updateInput(currency, isCurrencyMain)
        Glide
            .with(context)
            .load(getFlagRes(context))
            .into(flagImage)
        titleView.setText(getTitleRes(context))
        detailsView.setText(getDetailsRes(context))
    }

    fun updateInput(currency: Currency, isCurrencyMain: Boolean) = currency.apply {
        notify = false
        currencyEditText.let {
            val selectionStart = it.selectionStart
            val selectionEnd = it.selectionEnd
            val normalizedInputValue = if (inputValue.toDouble() != 0.0)
                inputValue.toPlainString()
            else
                ""

            it.setText(normalizedInputValue)
            if (selectionStart in normalizedInputValue.indices
                && selectionEnd in normalizedInputValue.indices)
                it.setSelection(selectionStart, selectionEnd)

            if (!isCurrencyMain) clearFocus()
        }
        notify = true
    }
}