package com.itworksmobile.android.currencyconverter.api.callback

import android.net.ConnectivityManager
import android.net.Network
import com.itworksmobile.android.currencyconverter.extension.isConnected

/**
 * Created by Naglis Žemaitis on 2019-10-19.
 * ItWorks Mobile. http://itworksmobile.com/
 */
class NetworkCallback(
    private val connectivityManager: ConnectivityManager,
    private val connectionCallback: (isConnected: Boolean) -> Unit
) : ConnectivityManager.NetworkCallback() {

    private var isConnected: Boolean = connectivityManager.isConnected()

    override fun onAvailable(network: Network?) {
        if (isConnected) return

        isConnected = true
        connectionCallback(isConnected)
    }

    override fun onLost(network: Network?) {
        if (!isConnected || connectivityManager.isConnected()) return

        isConnected = false
        connectionCallback(isConnected)
    }
}