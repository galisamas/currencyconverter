package com.itworksmobile.android.currencyconverter.feature.base

import android.view.ViewGroup
import androidx.annotation.CallSuper
import androidx.recyclerview.widget.RecyclerView
import com.itworksmobile.android.currencyconverter.feature.base.BaseAdapter.BaseViewType.SINGLE

/**
 * Created by Naglis Žemaitis on 2019-10-19.
 * ItWorks Mobile. http://itworksmobile.com/
 */

@Suppress("UNCHECKED_CAST")
abstract class BaseAdapter<ListItem> : RecyclerView.Adapter<BaseHolder<ListItem>>() {

    private var onViewClickListeners = mutableMapOf<Enum<*>, (viewId: Int, data: Any) -> Unit>()

    var items: MutableList<ListItem> = mutableListOf()
        set(value) {
            field = value.toMutableList()
            notifyDataSetChanged()
        }

    abstract fun <VH : BaseHolder<ListItem>> getViewHolder(parent: ViewGroup, viewType: Enum<*>): VH

    final override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseHolder<ListItem> {
        val viewTypeEnum = getViewTypeEnum(viewType)
        val viewHolder = getViewHolder<BaseHolder<ListItem>>(parent, viewTypeEnum)
        val clickListenerPos = onViewClickListeners.keys.firstOrNull { it == viewTypeEnum }
        clickListenerPos?.let {
            viewHolder.onViewClickListeners = { int, any ->
                onViewClickListeners[clickListenerPos]?.invoke(int, any)
            }
        }

        return viewHolder
    }

    override fun getItemViewType(position: Int): Int {
        val item = items[position]
        return if (item is BaseItem<*>)
            item.viewType.ordinal
        else
            super.getItemViewType(position)
    }

    @CallSuper
    override fun onBindViewHolder(holder: BaseHolder<ListItem>, position: Int) {
        holder.onBind(items[position])
    }

    override fun getItemCount(): Int = items.size

    open fun getViewTypeEnum(ordinal: Int): Enum<*> = SINGLE

    fun <D> addOnViewTypeViewClickListener(viewType: Enum<*>, callback: (viewId: Int, data: D) -> Unit) {
        onViewClickListeners[viewType] = { int, any ->
            callback.invoke(int, any as D)
        }
    }

    fun <T> getItemModel(index: Int) = items.map { it as? BaseItem<T> }[index]?.item

    open class BaseItem<ListItem>(val viewType: Enum<*>, var item: ListItem)

    open class SingleTypeItem<ListItem>(item: ListItem): BaseItem<ListItem>(SINGLE, item)

    enum class BaseViewType {
        SINGLE
    }
}