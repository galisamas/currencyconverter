package com.itworksmobile.android.currencyconverter.extension

import java.math.BigDecimal

/**
 * Created by Naglis Žemaitis on 2019-10-20.
 * ItWorks Mobile. http://itworksmobile.com/
 */

private const val RATE_SCALE = 2

fun newBigDecimal(value: Double) = BigDecimal(value)
    .setScale(RATE_SCALE, BigDecimal.ROUND_HALF_UP)
    .stripTrailingZeros()