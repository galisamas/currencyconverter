package com.itworksmobile.android.currencyconverter.feature.launch

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.itworksmobile.android.currencyconverter.R
import com.itworksmobile.android.currencyconverter.feature.converter.CurrencyConverterActivity

class LaunchActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        openCurrencyConverterScreen()
    }

    private fun openCurrencyConverterScreen() =
        startActivity(CurrencyConverterActivity.newIntent(this))
}
