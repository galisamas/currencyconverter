package com.itworksmobile.android.currencyconverter.api.callback

import android.util.Log
import com.itworksmobile.android.currencyconverter.BuildConfig
import com.itworksmobile.android.rallyapp.api.ApiResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by Naglis Žemaitis on 2019-09-23.
 * ItWorks Mobile. http://itworksmobile.com/
 */
class ApiCallback<ResponseClass>(val call: Call<ResponseClass>) : Callback<ResponseClass> {

    private lateinit var callback: (ApiResponse<ResponseClass>) -> Unit

    override fun onResponse(call: Call<ResponseClass>, response: Response<ResponseClass>) {
        val apiResponse = ApiResponse(response)
        if (BuildConfig.DEBUG) {
            apiResponse.get(success = {
                Log.i("", "${call.request().method()} ${call.request().url()} WS Response: ${apiResponse.responseCode} Successful!")
            }, failure = {
                Log.e("", "${call.request().method()} ${call.request().url()} WS Response: ${apiResponse.responseCode} Failure")
            })
        }
        callback(apiResponse)
    }

    override fun onFailure(call: Call<ResponseClass>, trowable: Throwable) {
        val apiResponse = ApiResponse<ResponseClass>(trowable)
        callback(apiResponse)

        Log.e("", "${call.request().method()} ${call.request().url()} WS Response: ${apiResponse.responseCode} Failure")
    }

    fun enqueue(callback: (ApiResponse<ResponseClass>) -> Unit) {
        this.callback = callback
        call.enqueue(this)
    }
}