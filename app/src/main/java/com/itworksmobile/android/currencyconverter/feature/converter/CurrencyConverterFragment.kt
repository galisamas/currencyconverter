package com.itworksmobile.android.currencyconverter.feature.converter

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import com.itworksmobile.android.currencyconverter.R
import com.itworksmobile.android.currencyconverter.extension.hideSoftwareInput
import com.itworksmobile.android.currencyconverter.extension.isConnected
import com.itworksmobile.android.currencyconverter.extension.showToast
import com.itworksmobile.android.currencyconverter.feature.base.BaseAdapter
import com.itworksmobile.android.currencyconverter.feature.base.BaseFragment
import com.itworksmobile.android.currencyconverter.feature.converter.adapter.CurrencyAdapter
import kotlinx.android.synthetic.main.fragment_currency_converter.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

/**
 * Created by Naglis Žemaitis on 2019-10-19.
 * ItWorks Mobile. http://itworksmobile.com/
 */
class CurrencyConverterFragment: BaseFragment() {

    private val viewModel: CurrencyConverterViewModel by sharedViewModel()
    private val adapter by lazy { CurrencyAdapter() }

    override val fragmentLayoutRes: Int
        get() = R.layout.fragment_currency_converter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViewModel()
        initAdapter()
    }

    private fun initAdapter() = adapter.apply {
        addOnViewTypeViewClickListener<Any>(BaseAdapter.BaseViewType.SINGLE) { viewId, data ->
            when(viewId) {
                R.id.currencyView -> {
                    (data as? Int)?.let { position ->
                        viewModel.updateSelectedCurrency(position)
                        adapter.moveItem(position)
                        activity?.hideSoftwareInput()
                    }
                }
                R.id.currencyEditText -> {
                    (data as? String)?.let { changedText ->
                        val currentRates = viewModel.updateSelectedCurrencyInput(changedText)
                        adapter.updateItems(currentRates)
                    }
                }
            }
        }
        recyclerView.adapter = this
    }

    private fun initViewModel() = viewModel.apply {
        onCurrencyChangedListener.observe(viewLifecycleOwner, Observer { currencyRates ->
            loadingView.isVisible = false
            if (adapter.itemCount == 0)
                adapter.setCurrencyItems(currencyRates)
            else
                adapter.updateItems(currencyRates)
        })
        onFailureListener.observe(viewLifecycleOwner, Observer {
            if (connectivityManager?.isConnected() == true)
                context?.showToast(R.string.endpoint_failure_alert)
        })
    }

    companion object {
        fun newInstance() = CurrencyConverterFragment()
    }
}