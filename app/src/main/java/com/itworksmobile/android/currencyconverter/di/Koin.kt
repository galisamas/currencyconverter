package com.itworksmobile.android.currencyconverter.di

import com.google.gson.Gson
import com.itworksmobile.android.currencyconverter.api.endpoint.CurrencyEndpoint
import com.itworksmobile.android.currencyconverter.feature.converter.CurrencyConverterViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Naglis Žemaitis on 2019-10-19.
 * ItWorks Mobile. http://itworksmobile.com/
 */
object Koin {

    private val retrofitModule = module(override = true) {
        single {
            Gson()
                .newBuilder()
                .create()
        }
        single {
            Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(get()))
                .baseUrl("https://revolut.duckdns.org/")
                .build()
        }
    }

    private val apiModule = module(override = true) {
        single { get<Retrofit>().create(CurrencyEndpoint::class.java) }
    }

    private val viewModelModule = module(override = true) {
        viewModel { CurrencyConverterViewModel(get()) }
    }

    val modules = listOf(retrofitModule, apiModule, viewModelModule)
}