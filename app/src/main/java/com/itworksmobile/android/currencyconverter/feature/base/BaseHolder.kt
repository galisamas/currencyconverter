package com.itworksmobile.android.currencyconverter.feature.base

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.extensions.LayoutContainer

/**
 * Created by Naglis Žemaitis on 2019-10-19.
 * ItWorks Mobile. http://itworksmobile.com/
 */
abstract class BaseHolder<ListItem>(itemView: View)
    : RecyclerView.ViewHolder(itemView), LayoutContainer {

    // Do not use this from subclasses, use onViewClick to deliver click actions
    internal var onViewClickListeners: (viewId: Int, data: Any) -> Unit = { _, _ -> }

    override val containerView: View = itemView

    val context: Context
        get() = itemView.context

    constructor(parent: ViewGroup, @LayoutRes layoutRes: Int) : this(
        LayoutInflater.from(parent.context).inflate(layoutRes, parent, false)
    )

    abstract fun onBind(listItem: ListItem)

    fun onViewClick(@IdRes viewId: Int) = onViewClick(viewId, Any())

    fun <D : Any> onViewClick(@IdRes viewId: Int, data: D) =
        onViewClickListeners.invoke(viewId, data)
}