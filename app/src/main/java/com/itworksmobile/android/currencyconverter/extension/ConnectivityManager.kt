package com.itworksmobile.android.currencyconverter.extension

import android.net.ConnectivityManager

/**
 * Created by Naglis Žemaitis on 2019-10-19.
 * ItWorks Mobile. http://itworksmobile.com/
 */

fun ConnectivityManager.isConnected() = activeNetworkInfo?.isConnected == true