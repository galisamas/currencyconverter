package com.itworksmobile.android.currencyconverter.extension

import android.app.Activity
import android.content.Context.INPUT_METHOD_SERVICE
import android.view.View
import android.view.inputmethod.InputMethodManager

/**
 * Created by Naglis Žemaitis on 2019-10-24.
 * ItWorks Mobile. http://itworksmobile.com/
 */

fun Activity.hideSoftwareInput() {
    val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
    val windowToken = currentFocus?.windowToken ?: View(this).windowToken
    imm.hideSoftInputFromWindow(windowToken, 0)
}