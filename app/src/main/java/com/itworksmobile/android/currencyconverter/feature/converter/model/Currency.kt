package com.itworksmobile.android.currencyconverter.feature.converter.model

import android.content.Context
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.itworksmobile.android.currencyconverter.R
import com.itworksmobile.android.currencyconverter.extension.getIdentifier
import java.math.BigDecimal
import java.util.*

/**
 * Created by Naglis Žemaitis on 2019-10-19.
 * ItWorks Mobile. http://itworksmobile.com/
 */
data class Currency(
    val abbreviation: String,
    var rate: BigDecimal,
    var inputValue: BigDecimal
) {

    private val resourceName by lazy { abbreviation.toLowerCase(Locale.getDefault()) }

    fun getTitleRes(context: Context) =
        getStringRes(context, "$resourceName$TITLE_POSTFIX", DEFAULT_TITLE)

    fun getDetailsRes(context: Context) =
        getStringRes(context, "$resourceName$DETAILS_POSTFIX", DEFAULT_DETAILS)

    fun getFlagRes(context: Context) =
        getDrawableRes(context, "$resourceName$DRAWABLE_POSTFIX", DEFAULT_DRAWABLE)

    @Suppress("SameParameterValue")
    private fun getDrawableRes(context: Context, resName: String, @DrawableRes fallbackRes: Int) =
        context.getIdentifier(resName, DRAWABLE_RESOURCES, fallbackRes)

    private fun getStringRes(context: Context, resName: String, @StringRes fallbackRes: Int) =
        context.getIdentifier(resName, STRING_RESOURCES, fallbackRes)

    companion object {
        private const val STRING_RESOURCES = "string"
        private const val TITLE_POSTFIX = "_abbreviation"
        private const val DETAILS_POSTFIX = "_full_title"
        private const val DRAWABLE_RESOURCES = "drawable"
        private const val DRAWABLE_POSTFIX = "_flag"

        private const val DEFAULT_DRAWABLE = R.drawable.ic_unknown
        private const val DEFAULT_TITLE = R.string.unknown_abbreviation
        private const val DEFAULT_DETAILS = R.string.unknown_full_title
    }
}