package com.itworksmobile.android.currencyconverter.feature.converter

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.itworksmobile.android.currencyconverter.R
import com.itworksmobile.android.currencyconverter.extension.replace
import com.itworksmobile.android.currencyconverter.feature.base.BaseActivity
import kotlinx.android.synthetic.main.activity_currency_converter.*
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * Created by Naglis Žemaitis on 2019-10-19.
 * ItWorks Mobile. http://itworksmobile.com/
 */
class CurrencyConverterActivity : BaseActivity() {

    private val viewModel: CurrencyConverterViewModel by viewModel()
    private val fragment by lazy { CurrencyConverterFragment.newInstance() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_currency_converter)

        initContainer()
    }

    override fun onResume() {
        super.onResume()
        viewModel.startCurrencyUpdates()
    }

    override fun onPause() {
        super.onPause()
        viewModel.terminateUpdates()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    private fun initContainer() = replace(frameContainer.id, fragment)

    companion object {
        fun newIntent(context: Context) =
            Intent(context, CurrencyConverterActivity::class.java)
    }
}