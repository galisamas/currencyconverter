package com.itworksmobile.android.currencyconverter.api.endpoint

import com.itworksmobile.android.currencyconverter.api.entity.CurrencyRatesResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Naglis Žemaitis on 2019-10-19.
 * ItWorks Mobile. http://itworksmobile.com/
 */
interface CurrencyEndpoint {

    @GET("latest")
    fun getLatestRates(@Query("base") base: String): Call<CurrencyRatesResponse>

}